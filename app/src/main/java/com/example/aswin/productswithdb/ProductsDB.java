package com.example.aswin.productswithdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductsDB {
    private DBHelper dbHelper;
    private Context ctx;

    public ProductsDB(Context ctx) {
        this.ctx = ctx;
        dbHelper = new DBHelper(ctx);
    }

    public Product getProduct(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = "id = ?";
        String[] selectionArgs = {"" + id};
        Cursor cursor = db.query(DBHelper.TABLE_PRODUCTS, null,
                selection, selectionArgs, null, null, null);

        //for more than one row results
//        while (cursor.moveToNext()) {
//            //read
//        }

        //for one row result
        cursor.moveToFirst();

        Product product = new Product();
        product.id = cursor.getInt(cursor.getColumnIndex(DBHelper.FIELD_PRODUCT_ID));
        product.name = cursor.getString(cursor.getColumnIndex(DBHelper.FIELD_PRODUCT_NAME));
        product.quantity = cursor.getInt(cursor.getColumnIndex(DBHelper.FIELD_PRODUCT_QUANTITY));

        cursor.close();
        db.close();
        return product;
    }

    public void insertProduct(Product product) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.FIELD_PRODUCT_NAME, product.name);
        cv.put(DBHelper.FIELD_PRODUCT_QUANTITY , product.quantity);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.insert(DBHelper.TABLE_PRODUCTS, null, cv);
        db.close();
    }
}
